/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronestrategia.estrategia;

/**
 *
 * @author ADMIN
 */
public class EstrategiaDivision implements IEstrategia{

    @Override
    public double algoritmo(double x, double y) {
        return (y!=0)?x/y:0;
    }
    
}
